//[SECTION] Dependencies and Modules
  const exp = require("express"); 
  const controller = require('./../controllers/users.js'); 

//[SECTION] Routing Component
  const route = exp.Router(); 

//[SECTION] Routes-[POST]
  route.post('/register', (req, res) => {
    let userDetails = req.body; 
    controller.registerUser(userDetails).then(outcome => {
       res.send(outcome);
    });
  });

//implement a route for the user authentication.
  //why POST => because this request setup will include a BODY SECTION, swo we cannot simply GET, GET ===> no body section
  route.post()

//[SECTION] Routes-[GET]
//[SECTION] Routes-[PUT]
//[SECTION] Routes-[DELETE]

//[SECTION] Expose Route System
  module.exports = route; 
