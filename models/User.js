//[SECTION] Deoendecies and MOdules
	const mongoose = require('mongoose');

//[SECTION] Blueprint Schema
	const userSchema = new mongoose.Schema({
		firstName:{
			type:String,
			required: [true, 'First Name is Required']
		},
		lastName:{
			type:String,
			required: [true, 'Last Name is Required']
		},
		email:{
			type:String,
			required: [true, 'Email is Required']
		},
		password:{
			type:String,
			required: [true, 'Password is Required']
		},
		gender:{
			type:String,
			required: [true, 'Gender is Required']
		},
		mobileNo:{
			type:String,
			required: [true, 'Gender is Required']
		},
		isAdmin:{
			type:Boolean,
			required: false
		},
		enrollments:[
			{
				courseID:{
					type: String,
					required:[true, 'Course ID is Required']
				},
				enrolledOn:{
					type: Date,
					default: new Date()
						},
				status:{
					type: String,
					default: 'Enrolled'
				}
			}

		]
		

	})



//[SECTION] Model
	//Translate the created schema into a mongoose model to be used as interface and perform the CRUD operations.
		//SYNTAX: model(<COLLECTION/MODEL NAME>,<SCHEMA>) 
		//User => users
	//for the produced model to become usable to other components within our app we will now exporse the data.
	const User = mongoose.model('User', userSchema)
	module.exports = User; //we exposed the User model for it to become usable and recognizable to other external modules within the app using the exports keyword.